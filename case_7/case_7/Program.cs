﻿//improver caching
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;
class Cache
{
    private  static Dictionary<int, object> _cache = new Dictionary<int,
   object>();
    public static void Add(int key, object value)
    {
        //_cache.Add(key, value); 
        //change to avoid same key wll causing error
        _cache[key]=value;

    }
    public static object Get(int key)
    {
        return _cache[key];
    }
}

class Program
{
    //add ConcurrenctDictionary FOR SAFE ACCESS 
    static int initialCapacity = 101;
    static int numProcs = Environment.ProcessorCount;
    static int concurrencyLevel = numProcs * 2;
    static ConcurrentDictionary<int, int> cd = new ConcurrentDictionary<int, int>(concurrencyLevel, initialCapacity);

    static void Main(string[] args)
    {

        for (int i = 0; i < 1000000; i++)
        {
            cd[i] = i;//add ConcurrentDictionary

            //Cache.Add(i, new object());
            //change to:
            Cache.Add(cd[i], new object());

            Console.WriteLine(JsonSerializer.Serialize(Cache.Get(i)).ToString());
        }
        Console.WriteLine("Cache populated");

        Console.ReadLine();
    }
}

