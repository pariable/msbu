$(function(){

    function textCaseStats(text) {
        return {
            upper: (text.match(/[a-z]/g) || []).length,
            lower: (text.match(/[A-Z]/g) || []).length
        };
    }
    
    function hasMixedCase(password) {
        var caseStats = textCaseStats(password);
    
        return caseStats.lower > 0 && caseStats.upper > 0;
    }

    function hasSpecialCharacter(password){
        var checkSpecial = /[*@!#%&()^~{}]+/.test(password);
        return checkSpecial;
    }

    function hasDigit(password){
        var check = /^(?=\D*\d)[a-zA-Z0-9 -]+$/.test(password);
        return check;
    }

    
    var Obj={
        Errors:[],
        validateLogin:function(){
            let data={
                "username":$('#txtUsername').val(),
                "password":$('#txtPassword').val()
            }
            if(data.username!="" && data.password!=""){
                return true;
            }else{
                return false;
            }
        },
        validateRegister:function(){

            
            let data={
                "MyName":$('#txtMyNameRegister').val(),
                "username":$('#txtUsernameRegister').val(),
                "password":$('#txtPasswordRegister').val()
            }
            if(data.MyName!="" && data.username!="" && data.password!=""){
                return true;
            }else{
                return false;
            }
        }
    }

    $('.btnLogin').off().on('click',function(e){
        if(Obj.validateLogin()){
           
           var datas= {
                username:$('#txtUsername').val(),
                password:$('#txtPassword').val()
            };
           
            $.ajax({
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                xhrFields: 'withCredentials:true',
                url: Api.Users.Login,
                crossOrigin: true,
                type: "post",
                data:JSON.stringify(datas),
                dataType:"json",
                success: function(data, textStatus, xhr) {

                    alert("Login Success!");
                    localStorage.setItem("id", data.datas.id);
                    localStorage.setItem("username", data.datas.username);
                    window.location.href="dashboard.html";
                },
                complete: function(xhr, textStatus) {
                    if(xhr.status==400){
                        alert("Masukan Account Anda atau lakukan registrasi");
                    }
                } 
            })
           
        }else{
            alert("Masukan Account Anda atau lakukan registrasi");
        }

        e.preventDefault();
    })

    $('.actConfirmRegister').click(function(e){

        //reset
        Obj.Errors=[];
        $('#txtError').empty();

        if(!hasMixedCase($('#txtPasswordRegister').val())){
            Obj.Errors.push("Password harus berisi setidak nya 1 huruf Besar dan 1 huruf Kecil!");
            console.log(Obj.Errors);
            
        }

        if(!hasSpecialCharacter($('#txtPasswordRegister').val())){
            Obj.Errors.push("Password harus berisi setidak nya 1 special character");
            console.log(Obj.Errors);
            
        }

        if($('#txtPasswordRegister').val().length < 8){
            Obj.Errors.push("Password harus berisi setidak nya 8 character");
            console.log(Obj.Errors);
        }

        if(!/\d/.test($('#txtPasswordRegister').val())){
            Obj.Errors.push("Password harus berisi setidak nya 1 number");
            console.log(Obj.Errors);
        }
        

        //passed all check regex
        if(Obj.Errors.length==0){
            if(Obj.validateRegister()){
                $('.actConfirmRegister').attr('disabled','disabled').text('Processing ...');
                var datas= {
                    MyName:$('#txtMyNameRegister').val(),
                    username:$('#txtUsernameRegister').val(),
                    password:$('#txtPasswordRegister').val()
                };
               
                $.ajax({
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    xhrFields: 'withCredentials:true',
                    url: Api.Users.Register,
                    crossOrigin: true,
                    type: "post",
                    data:JSON.stringify(datas),
                    dataType:"json",
                    success: function(data, textStatus, xhr) {
    
                        //if succes
                        $('.actConfirmRegister').removeAttr('disabled').text('Register');

                        $('#viewModalDemo').modal();
                        $('.vLogin').hide();
                        $('.vRegister').hide();
                        $('.vPass').hide();
                        $('.vConfirmRegister').show();
                        $('.vConfirmPass').hide();

                        $('#txtMyNameRegister').val('');
                        $('#txtUsernameRegister').val('');
                        $('#txtPasswordRegister').val('');
    
                    },
                    complete: function(xhr, textStatus) {
                        if(xhr.status==400){
                            $('.actConfirmRegister').removeAttr('disabled').text('Register');
                            alert("Mungkin akun sudah ter register!");
                        }
                    } 
                })
    
               
                
            }else{
                alert("Masukan Detail Field!");
            }    

        }else{
            let r="<ul>";
            $.each(Obj.Errors,function(i,e){
                r+="<li>"+e+"</li>";
            })
            r+="</ul>";
            $('#txtError').empty().html(r);
        }
        
        
        e.preventDefault();

        
      });
})