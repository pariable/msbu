$(function(){
    let uname = localStorage.getItem("username");
    $('.title-content-account').text(uname);

    async function ShowDefault(){
        if(uname!="admin"){
            Api.getDocumentByUser(function(){
                var el="";
                let no=1;
                if(typeof Api.Response !='undefined' && Api.Response.datas.length > 0){
                    el+="<table id='tblDashboard' class='table table-striped table-bordered' style='width:100%'>";
                        el+="<thead>";
                            el+="<tr>";
                                el+="<td>No</td>";
                                el+="<td>ID</td>";
                                el+="<td>Owner</td>";
                                el+="<td>File Xls</td>";
                                el+="<td>File Pdf</td>";
                                
                            el+="</tr>";
                        el+="</thead>";
                    el+="<tbody>";
                        $.each(Api.Response.datas,function(i,e){
                            el+="<tr>";
                                el+="<td>"+no+"</td>";
                                el+="<td>"+e.id+"</td>";
                                el+="<td>"+e.owner+"</td>";
                                el+="<td><a href='"+Api.Document.Download+"?file="+e.fileXls+"' class='btnDownload' data-file='"+e.fileXls+"' download>"+e.fileXls+"</a></td>";
                                el+="<td><a href='"+Api.Document.Download+"?file="+e.filePdf+"' class='btnDownload' data-file='"+e.filePdf+"'>"+e.filePdf+"</a></td>";
                            el+="</tr>";
                            no++;
        
                        });
                    el+="</tbody>";
                    el+="</table>";
                    
                }else{
                    el+="<div class='alert alert-success alert-dismissible' style='width:100%;'>";
                        el+="<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>";
                        el+="You do not have any files";
                    el+="</div>";
        
                }
        
                $('#mainContent').empty().html(el);
                $('#tblDashboard').dataTable({
                    pageLength : 3,
                    lengthMenu: [[5, 10, 20, -1], [5, 10, 20, 'Todos']]
                });
        
            });
    
        }else{
            Api.getAllDocuments(function(){
                var el="";
                let no=1;
                if(typeof Api.Response !='undefined' && Api.Response.datas.length > 0){
                    el+="<table id='tblDashboard' class='table table-striped table-bordered' style='width:100%'>";
                        el+="<thead>";
                            el+="<tr>";
                                el+="<td>No</td>";
                                el+="<td>ID</td>";
                                el+="<td>Owner</td>";
                                el+="<td>File Xls</td>";
                                el+="<td>File Pdf</td>";
                                
                            el+="</tr>";
                        el+="</thead>";
                    el+="<tbody>";
                        $.each(Api.Response.datas,function(i,e){
                            el+="<tr>";
                                el+="<td>"+no+"</td>";
                                el+="<td>"+e.id+"</td>";
                                el+="<td>"+e.owner+"</td>";
                                el+="<td><a href='"+Api.Document.Download+"?file="+e.fileXls+"' class='btnDownload' data-file='"+e.fileXls+"' download>"+e.fileXls+"</a></td>";
                                el+="<td><a href='"+Api.Document.Download+"?file="+e.filePdf+"' class='btnDownload' data-file='"+e.filePdf+"'>"+e.filePdf+"</a></td>";
                            el+="</tr>";
                            no++;
        
                        });
                    el+="</tbody>";
                    el+="</table>";
                    
                }else{
                    el+="<div class='alert alert-success alert-dismissible' style='width:100%;'>";
                        el+="<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>";
                        el+="You do not have any files";
                    el+="</div>";
        
                }
        
                $('#mainContent').empty().html(el);
                $('#tblDashboard').dataTable({
                    pageLength : 5,
                    lengthMenu: [[5, 10, 20, -1], [5, 10, 20, 'Todos']]
                });
        
            });
        }

    }

    ShowDefault();

    async function uploadFile(e) {
        var xls=$('#fileXls').get(0).files[0];
        var pdf=$('#filePdf').get(0).files[0];
       
        
        let form_data = new FormData(); 
        form_data.append('FileXls', xls);
        form_data.append('FilePdf', pdf);
        form_data.append('IdUser', localStorage.getItem('id'));

        await fetch(Api.Document.Upload, {
            method: "POST", 
            body: form_data
        })
        .then(response => response.json())
        .then(data => {
            e();
            $('.btnUpload').removeAttr('disabled').text('Upload');
            const posts = data;
            if(posts.status==400){
                alert("File belum dipilih!")
            }else{

                $('.fa-times').trigger('click');
                $('#fileXls').val('');
                $('#filePdf').val('')
                alert("File sukses di upload!")
            }
        })
        .catch(error => alert("File belum dipilih!")); 
        
    }

    $('#fileXls').bind('change', function() {
        let kb =this.files[0].size/1000;
        if(this.files[0].size > 1000000){
            alert("Ukuran file terlalu besar , lebih dari 1 GB");
            $(this).val('');
        }
        
    });

    $('#filePdf').bind('change', function() {
        let kb =this.files[0].size/1000;
        if(this.files[0].size > 1000000){
            alert("Ukuran file terlalu besar , lebih dari 1 GB");
            $(this).val('');
        }
    
    });


    $('.btnUpload').off().on('click',function(e){
        $(this).attr('disabled','disabled').text('Processing ...');
        uploadFile(function(){
            ShowDefault();
        });

        return false;
        
    });
   
})