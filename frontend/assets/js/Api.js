var Api={
    Response:{},
    Users:  {
        All         :   "https://localhost:44398/api/v1/user",
        Login       :   "https://localhost:44398/api/v1/user/login",
        Register    :   "https://localhost:44398/api/v1/user/register"
    },
    Document:{
        All         :   "https://localhost:44398/api/v1/document",
        ByUser      :   "https://localhost:44398/api/v1/document/byUser",    
        Upload      :   "https://localhost:44398/api/v1/document/upload",
        Download    :   "https://localhost:44398/api/v1/document/download"
    },
    getAllUser:function(e){
        $.ajax({
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            crossDomain : true,
            url         : Api.Users.All,
            crossOrigin : true,
            type        : "get",
            success     : function(data) {
                console.log(data)
            }
        })
    
    },
    getAllDocuments:function(e){
        $.ajax({
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            crossDomain : true,
            url         : Api.Document.All,
            crossOrigin : true,
            type        : "get",
            success     : function(data) {
                Api.Response=data;
                e();
                console.log(data)
            }
        })
    
    },
    getDocumentByUser:function(e){
        $.ajax({
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            crossDomain : true,
            url         : Api.Document.ByUser+"?idUser="+localStorage.getItem("id"),
            crossOrigin : true,
            type        : "get",
            success     : function(data) {
                Api.Response=data;
                e();
                console.log(data)
            }
        })
    
    },
    DownloadFile:function(file,e){
        
        $.ajax({
            crossDomain : true,
            url         : Api.Document.ByUser+"?file="+file,
            contentType: false, // Not to set any content header  
            processData: false, // Not to process data 
            crossOrigin : true,
            type        : "get",
            success     : function(data) {
                Api.Response=data;
                e();
                console.log(data)
            }
        })
    }

}