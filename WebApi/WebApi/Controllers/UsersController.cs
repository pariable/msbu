﻿using WebApi.Models;
using WebApi.Services;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using Microsoft.AspNetCore.Cors;

namespace WebApi.Controllers;

[Route("api/v1/user")]
[ApiController]
[EnableCors("MSBUOrigin")]
public class UsersController : ControllerBase
{
    private readonly UserService userService;

    public UsersController(UserService userService)
    {
        this.userService = userService;
    }

    [HttpGet("{id}")]
    public ActionResult<Users> Get(int id)
    {
        var result = userService.Get(id);
        return Ok(result);
    }

    [HttpGet]
    public ActionResult<List<Users>> GetList()
    {
        var result = userService.GetList();
        return Ok(result);
    }

    
    [HttpPut("{id}")]
    public ActionResult<Users> Update(int id, [FromBody] Users user)
    {
        user.Id = id;
        var result = userService.Update(user);
        return Ok(result);
    }

    [HttpDelete("{id}")]
    public ActionResult Delete(int id)
    {
        userService.Delete(id);
        return NoContent();
    }

    [HttpPost]
    [Route("login")]
    
    public  ActionResult<Users> Login(Users user)
    {
        //check if exist to avoid duplicate username
        var check = userService.GetLogin(user);

        if (!check)
        {
            
            return StatusCode(400, new { status = false, message = "User not found", datas = "" });

        }

        return   StatusCode(200, new {status=true, message = "Login Successfully!",datas=userService.GetByEmail(user) });
        
    }

    [HttpPost]
    [Route("register")]
    public ActionResult<Users> Create([FromBody] Users user)
    {
        //check if the user has been registered or not
        var check = userService.CheckIfUserExist(user);

        if (!check)
        {
            userService.Create(user);
            return StatusCode(200, new { status = true, message = user.Username + " has been created!", datas = user.Username });
        }
        return StatusCode(400, new { status = false, message = "User has been registered before", datas = "" });


    }

}
