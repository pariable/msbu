﻿using WebApi.Models;
using WebApi.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;

namespace WebApi.Controllers;

[Route("api/v1/document")]
[ApiController]
public class DocumentsController : ControllerBase
{
    private readonly DocumentService documentService;

    public DocumentsController(DocumentService documentService)
    {
        this.documentService = documentService;
    }

    [HttpGet("{id}")]
    public ActionResult<Documents> Get(int id)
    {
        var result = documentService.Get(id);
        return Ok(result);
    }

    [HttpGet]
    [Route("byUser")]
    public ActionResult<Documents> GetByUserId(int idUser)
    {
        var result =  documentService.GetByUserId(idUser);

        if(result.Count > 0)
        {
            return StatusCode(200, new { status = true, message = "Ok", datas = result });
        }
        else
        {
            return StatusCode(204, new { status = false, message = "No datas", datas = result });
        }
        
    }

    [HttpGet]
    public ActionResult<List<Documents>> GetList()
    {
        var result = documentService.GetList();

        if (result.Count > 0)
        {
            return StatusCode(200, new { status = true, message = "Ok", datas = result });
        }
        else
        {
            return StatusCode(204, new { status = false, message = "No datas", datas = result });
        }

        
    }

    [HttpDelete("{id}")]
    public ActionResult Delete(int id)
    {
        documentService.Delete(id);
        return NoContent();
    }

    [HttpPost]
    [Route("upload")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(string),StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> Upload([FromForm]Upload upload)
    {
        try
        {
            var fileXls = await writeFile(upload.FileXls);
            var filePdf = await writeFile(upload.FilePdf);

            Documents doc = new Documents();
            doc.IdUser = int.Parse(upload.IdUser);
            doc.FileXls = fileXls;
            doc.FilePdf = filePdf;

            //save to db
            documentService.Create(doc);

            return Ok(new { result=true,message="Upload files was succesfully!",xls= fileXls ,pdf= filePdf});

        }catch(Exception e)
        {
            throw e;
        }
       
    }

    [HttpGet]
    [Route("download")]
    public async Task<IActionResult> Download(string file)
    {
        var filepath = Path.Combine(Directory.GetCurrentDirectory(), "Upload\\Files",file);
        var provider = new FileExtensionContentTypeProvider();
        if(!provider.TryGetContentType(filepath,out var contentType))
        {
            contentType = "application/octet-stream";
        }

        var bytes = await System.IO.File.ReadAllBytesAsync(filepath);
        return File(bytes, contentType, Path.GetFileName(filepath));

    }

    private async Task<string> writeFile(IFormFile file)
    {
        var filename = "";
        try
        {
            var extention  = "." + file.FileName.Split('.')[file.FileName.Split('.').Length - 1];
            filename = DateTime.Now.Ticks.ToString() + extention;

            var filepath = Path.Combine(Directory.GetCurrentDirectory(), "Upload\\Files");

            if(!Directory.Exists(filepath))
            {
                Directory.CreateDirectory(filepath);
            }

            //chunk method
            int chunkSize = 1024 * 1024 * 5;
            var exactpath = Path.Combine(Directory.GetCurrentDirectory(), "Upload\\Files", filename);
            
            using (FileStream streamx = new FileStream(exactpath, FileMode.Create))
            {
                byte[] buffer = new byte[chunkSize];

                int bytesRead = 0;
                long bytesToRead = streamx.Length;

                while (bytesToRead > 0)
                {

                    int n = streamx.Read(buffer, 0, chunkSize);

                    if (n == 0) break;

                    // Let's resize the last incomplete buffer
                    if (n != buffer.Length)
                        Array.Resize(ref buffer, n);

                    await file.CopyToAsync(streamx);

                    bytesRead += n;
                    bytesToRead -= n;

                }


                
            }

            return filename;


        }catch(Exception e)
        {
            throw e;
        }

        return filename;

    }

   
}
