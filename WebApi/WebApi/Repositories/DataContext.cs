﻿using WebApi.Models;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Repositories;

public class DataContext : DbContext
{
    public DataContext(DbContextOptions options) : base(options) { }

    public DbSet<Users> Users { get; set; }


    public DbSet<Documents> Documents { get; set; }
}
