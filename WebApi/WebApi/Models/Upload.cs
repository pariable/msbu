﻿namespace WebApi.Models
{
    public class Upload
    {
        public string IdUser { get; set; }
        public IFormFile FileXls{ get; set; }
        public IFormFile FilePdf { get; set; }
    }
}
