﻿namespace WebApi.Models;

public class Documents
{
    public int Id { get; set; }
    public int IdUser { get; set; }

    public string FileXls { get; set; }
    public string FilePdf { get; set; }
    
}
