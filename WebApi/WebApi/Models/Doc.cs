﻿namespace WebApi.Models
{
    public class Doc
    {
        public int Id { get; set; }
        public int IdUser { get; set; }

        public string Owner { get; set; }
        public string FileXls { get; set; }
        public string FilePdf { get; set; }
    }
}
