﻿using WebApi.Models;
using WebApi.Repositories;

namespace WebApi.Services;

public class UserService
{
    private readonly DataContext db;

    public UserService(DataContext db)
    {
        this.db = db;
    }

    public Users Get(int id)
    {
        return db.Users.Find(id);
    }

    public Users GetByEmail(Users user)
    {
        return db.Users.FirstOrDefault(u => u.Username == user.Username && u.Password == user.Password);
    }

    public Boolean CheckIfUserExist(Users user)
    {
        var result = db.Users.FirstOrDefault(u => u.Username == user.Username  );
        return (result != null) ? true : false;
    }

    public Boolean GetLogin(Users user)
    {
        var login = db.Users.First(u => u.Username == user.Username && u.Password == user.Password);
        return (login != null) ? true : false;
    }

    public List<Users> GetList()
    {
        return db.Users.ToList();
    }

    public Users Create(Users user)
    {
        db.Users.Add(user);
        db.SaveChanges();
        return user;
    }

    public Users Update(Users user)
    {
        db.Users.Update(user);
        db.SaveChanges();
        return user;
    }

    public void Delete(int id)
    {
        var user = db.Users.Find(id);
        db.Users.Remove(user);
        db.SaveChanges();
    }
}
