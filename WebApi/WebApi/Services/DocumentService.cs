﻿using Microsoft.EntityFrameworkCore;
using WebApi.Models;
using WebApi.Repositories;

namespace WebApi.Services;

public class DocumentService
{
    private readonly DataContext db;

    public DocumentService(DataContext db)
    {
        this.db = db;
    }

    public Documents Get(int id)
    {
        return db.Documents.Find(id);
    }

    public List<Doc> GetByUserId(int id)
    {
        var query = (from d in db.Documents
                    join u in db.Users on d.IdUser equals u.Id
                    where d.IdUser == id
                    select new Doc
                    {
                        Id = d.Id,
                        IdUser = u.Id,
                        Owner = u.MyName,
                        FileXls = d.FileXls,
                        FilePdf = d.FilePdf
                    })
                    .OrderByDescending(x => x.Id);

        return query.ToList();

    }

    public List<Doc> GetList()
    {
        var query = (from d in db.Documents
                     join u in db.Users on d.IdUser equals u.Id
                     select new Doc
                     {
                         Id = d.Id,
                         IdUser = u.Id,
                         Owner = u.MyName,
                         FileXls = d.FileXls,
                         FilePdf = d.FilePdf
                     })
                     .OrderByDescending(x => x.Id);

        return query.ToList();
    }

    public Documents Create(Documents document)
    {
        db.Documents.Add(document);
        db.SaveChanges();
        return document;
    }

    public Documents Update(Documents document)
    {
        db.Documents.Update(document);
        db.SaveChanges();
        return document;
    }

    public void Delete(int id)
    {
        var document = db.Documents.Find(id);
        db.Documents.Remove(document);
        db.SaveChanges();
    }
}
