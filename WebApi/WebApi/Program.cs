using WebApi.Repositories;
using Microsoft.EntityFrameworkCore;
using WebApi.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

var MyAllowSpecificOrigins = "MSBUOrigin";
var builder = WebApplication.CreateBuilder(args);


// configure DataContext
var connectionString = builder.Configuration.GetConnectionString("DataContext");
builder.Services.AddDbContext<DataContext>(options =>
    options.UseSqlServer(connectionString));

// configure Application Services
builder.Services.AddScoped<UserService>();
builder.Services.AddScoped<DocumentService>();

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(c =>
{
    c.ResolveConflictingActions(apiDescriptions => apiDescriptions.First());
});

//allowed CORS

builder.Services.AddCors(options =>
{
    options.AddPolicy(MyAllowSpecificOrigins,
                      policy =>
                      {
                          policy.WithOrigins("*")
                            .AllowAnyHeader()
                            .AllowAnyMethod()
                            .WithMethods("PUT", "DELETE", "GET","POST"); ;
                      });
});

builder.Services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

builder.Services.AddMvcCore(options =>
{
    options.RequireHttpsPermanent = true; // does not affect api requests
    options.RespectBrowserAcceptHeader = true; // false by default
}).AddFormatterMappings()
.AddJsonOptions(options =>
{
    options.JsonSerializerOptions.IncludeFields = true;
});



var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}



app.UseHttpsRedirection();
app.UseRouting();
app.UseCors(x => x.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());
app.UseAuthentication();
app.UseAuthorization();
app.UseEndpoints(endpoints =>
{
    endpoints.MapControllers();
});

app.MapControllers();

app.Run();
