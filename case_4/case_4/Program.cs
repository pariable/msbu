﻿//Keeping references to objects unnecessarily
namespace MemoryLeakExample
{
    class Program
    {
        static void Main(string[] args)
        {
            //soal No 4
            //var myList = new List(); ==> wrong declaration data type
            //solusi nya adalah ubah tipe data dengan menggunakan generic List
            //dengan menggunakan object Product sbb:

            List<Product> myList = new List<Product>();

            while (true)
            {
                // populate list with 1000 integers
                for (int i = 0; i < 1000; i++)
                {
                    myList.Add(new Product(Guid.NewGuid().ToString(), i));

                }
                // do something with the list object
                Console.WriteLine(myList.Count);
            }

        }

    }
    class Product
    {
        public Product(string sku, decimal price)
        {
            SKU = sku;
            Price = price;
        }
        public string SKU { get; set; }
        public decimal Price { get; set; }
    }

}