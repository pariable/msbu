﻿//Large object graphs

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace MemoryLeakExample
{
    class Program
    {
        static void Main(string[] args)
        {
            var rootNode = new TreeNode("1","test");
            while (true)
            {
                // create a new subtree of 10000 nodes
                var newNode = new TreeNode("2","edwin");
                for (int i = 0; i < 10; i++)
                {
                    var childNode = new TreeNode("3","warming");
                    
                    newNode.AddChild(childNode);
                    
                }
                rootNode.AddChild(newNode);

                string combinedString = JsonSerializer.Serialize(rootNode);
                
                Console.WriteLine(combinedString);

            }
        }
    }
    class TreeNode
    {
        private string Id { get; set; }
        private string Name { get; set; }

        public TreeNode(string a,string b)
        {
            Id = a;
            Name = b;
        }
        private readonly List<TreeNode> _children = new List<TreeNode>();
        public void AddChild(TreeNode child)
        {
            _children.Add(child);
        }
    }
}