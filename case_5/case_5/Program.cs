﻿
// event handlers
using System;
using System.Threading;
using System.Threading.Tasks;

namespace MemoryLeakExample
{
    class Program
    {
        static void Main(string[] args)
        {
            //var publisher = new EventPublisher();
            //change data type to object created
            EventPublisher publisher = new EventPublisher();

            while (true)
            {

                //var subscriber = new EventSubscriber(publisher);
                //change data type to object created 
                EventSubscriber subscriber = new EventSubscriber(publisher);
                // do something with the publisher and subscriber objects
                publisher.RaiseEvent();


            }

        }

    }

    class EventPublisher
    {

        public event EventHandler MyEvent;
        public virtual void RaiseEvent() //add protected virtual method
        {
            Console.WriteLine("Process Started!");
            MyEvent?.Invoke(this, EventArgs.Empty);
        }
    }
    class EventSubscriber
    {

        public EventSubscriber(EventPublisher publisher)
        {
            publisher.MyEvent += OnMyEvent;
        }
        private void OnMyEvent(object sender, EventArgs e)
        {
            Console.WriteLine("MyEvent raised");
        }
    }
}

    